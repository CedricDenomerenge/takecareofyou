package be.inforius.takecareofyou.business


import android.bluetooth.BluetoothManager
import android.bluetooth.le.AdvertiseCallback
import android.bluetooth.le.AdvertiseData
import android.bluetooth.le.AdvertiseSettings
import android.bluetooth.le.BluetoothLeAdvertiser
import android.content.Context
import android.os.ParcelUuid
import android.widget.Toast
import be.inforius.takecareofyou.domain.constant.BluetoothConstants
import be.inforius.takecareofyou.domain.constant.DBConstants
import java.util.*


class MyBluetoothGattServer(private val context: Context, private val mBluetoothManager: BluetoothManager ) {

    private var mBluetoothLeAdvertiser: BluetoothLeAdvertiser? = null


    /**
     * Callback to receive information about the advertisement process.
     */
    private val mAdvertiseCallback = object : AdvertiseCallback() {
        override fun onStartSuccess(settingsInEffect: AdvertiseSettings) {
            println("LE Advertise Started.")
            Toast.makeText(context, "LE Advertise Started.", Toast.LENGTH_SHORT).show()
        }

        override fun onStartFailure(errorCode: Int) {
            println("LE Advertise Failed: $errorCode")
            Toast.makeText(context, "LE Advertise Failed: $errorCode", Toast.LENGTH_SHORT).show()
        }
    }

    /**
     * Begin advertising over Bluetooth that this device is connectable
     * and supports the Current custom Service.
     */
    fun startAdvertising() : Boolean {

        val bluetoothAdapter = mBluetoothManager.adapter
        mBluetoothLeAdvertiser = bluetoothAdapter.bluetoothLeAdvertiser

        if (mBluetoothLeAdvertiser == null) {
            return false
        }

        val settings = AdvertiseSettings.Builder()
            .setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_LOW_POWER)
            .setConnectable(false)
            .setTimeout(0)
            .setTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_MEDIUM)
            .build()

        val data = AdvertiseData.Builder()
            .setIncludeDeviceName(true)
            .setIncludeTxPowerLevel(true)
            .addServiceUuid(ParcelUuid(UUID.fromString(BluetoothConstants.CUSTOM_SERVICE_UUID)))
            .build()

        mBluetoothLeAdvertiser!!.startAdvertising(settings, data, mAdvertiseCallback)

        return true
    }

    /**
     * Stop Bluetooth advertisements.
     */
    fun stopAdvertising() {
        if (mBluetoothLeAdvertiser == null) {
            return
        }
        mBluetoothLeAdvertiser!!.stopAdvertising(mAdvertiseCallback)
    }




}
