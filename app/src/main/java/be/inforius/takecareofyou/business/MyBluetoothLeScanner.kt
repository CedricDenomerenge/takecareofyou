package be.inforius.takecareofyou.business


import android.bluetooth.BluetoothAdapter
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanFilter
import android.bluetooth.le.ScanResult
import android.bluetooth.le.ScanSettings
import android.content.Context
import android.os.Build
import android.os.ParcelUuid
import android.widget.Toast
import be.inforius.takecareofyou.business.listener.OnScanCallbackListener
import be.inforius.takecareofyou.domain.constant.BluetoothConstants
import be.inforius.takecareofyou.domain.constant.DBConstants
import be.inforius.takecareofyou.model.entity.Record
import be.inforius.takecareofyou.view.view_model.RecordViewModel
import java.util.ArrayList
import java.util.UUID
import java.sql.DriverManager.println

class MyBluetoothLeScanner( private val context: Context,
                            private val bluetoothAdapter: BluetoothAdapter,
                            private val listener: OnScanCallbackListener) {


    fun startsScan() {

        //println("BLE is supported, start scanning...")

        // Depending the SDK version ( Oreo / 26+ or above )
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            val bluetoothLeScanner = bluetoothAdapter.bluetoothLeScanner
            // Stop first if running
            //bluetoothLeScanner.stopScan(mScanCallback)
            // Add some settings ( delay, mode )
            val settings = ScanSettings.Builder().setReportDelay(0).setScanMode(ScanSettings.SCAN_MODE_LOW_POWER).build()
            // Set filter with Custom UUID
            val build = ScanFilter.Builder().setServiceUuid(ParcelUuid(UUID.fromString(BluetoothConstants.CUSTOM_SERVICE_UUID))).build()
            val arrayList = ArrayList<ScanFilter>()
            arrayList.add(build)
            // Start scan
            bluetoothLeScanner.startScan(arrayList, settings, mScanCallback)
        } else {
            // Stop first if running
            //bluetoothAdapter.stopLeScan(mLeScanCallback)
            // Set filter with Custom UUID
            val serviceUuids = arrayOfNulls<UUID>(1)
            serviceUuids[0] = UUID.fromString(BluetoothConstants.CUSTOM_SERVICE_UUID)
            // Start scan
            bluetoothAdapter.startLeScan(serviceUuids, mLeScanCallback) // API 18+
        }


    }

    fun stopScan() {

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            val bluetoothLeScanner = bluetoothAdapter.bluetoothLeScanner
            bluetoothLeScanner.stopScan(mScanCallback)
        } else {
            bluetoothAdapter.stopLeScan(mLeScanCallback)
        }

    }

    private val mScanCallback = object : ScanCallback() {

        override fun onScanResult(callbackType: Int, result: ScanResult) {

            super.onScanResult(callbackType, result)

            val rssi = result.rssi.toDouble() //The intensity of the received signal
            var tx: Int? = null

            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O) {
                tx = result.txPower //The power of the broadcast (Available on Oreo only)
            }

            println("Found " + result.device.name + ", MAC  " + result.device.address + " RSSI: " + rssi + " dBm")

            /*Toast.makeText(context, "NEARBY DEVICE DETECTED\n\nNAME : " + result.device.name
                    + "\nMAC : " + result.device.address + "\nRSSI : " + rssi + "dBm\nTX : "
                    + tx, Toast.LENGTH_SHORT).show()*/

            val recordVM = RecordViewModel(result.timestampNanos, result.device.address,
                "${result.device.name}", result.rssi, tx)

            listener.displayData(recordVM)
        }
    }


    private val mLeScanCallback =

        BluetoothAdapter.LeScanCallback { device, rssi, scanRecord ->

            println("Found " + device.name + ", MAC  " + device.address + " RSSI: " + rssi + "dBm")

            /*Toast.makeText( context, "NEARBY DEVICE DETECTED\n\nNAME : " + device.name + "\nMAC : "
                    + device.address + "\nRSSI : " + rssi + " dBm", Toast.LENGTH_SHORT ).show()*/

            val recordVM = RecordViewModel(null, device.address,
                "${device.name}", rssi, null)

            listener.displayData(recordVM)
        }




}
