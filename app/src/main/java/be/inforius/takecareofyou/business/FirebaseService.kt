package be.inforius.takecareofyou.business

import be.inforius.takecareofyou.model.entity.Status
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class FirebaseService {

    companion object{

        private var mFirebaseInstance: FirebaseDatabase? = null
        private var mFirebaseDatabase: DatabaseReference? = null

        fun createRecords(){

            val status = Status()

            getDatabase().setValue(status)
        }

        fun createStatus(){

            val status = Status()

            getDatabase().setValue(status)
        }

        private fun getInstance(): FirebaseDatabase {
            if (mFirebaseInstance == null){
                mFirebaseInstance = FirebaseDatabase.getInstance()
                //(mFirebaseInstance as FirebaseDatabase).setPersistenceEnabled(true)
            }
            return mFirebaseInstance as FirebaseDatabase
        }

        private fun getDatabase(): DatabaseReference {
            if (mFirebaseDatabase == null){
                mFirebaseDatabase = getInstance().getReference("status")
                //(mFirebaseDatabase as DatabaseReference).keepSynced(false)
            }
            return mFirebaseDatabase as DatabaseReference
        }
    }
}