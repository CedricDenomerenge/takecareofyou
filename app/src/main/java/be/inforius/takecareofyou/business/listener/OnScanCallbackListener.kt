package be.inforius.takecareofyou.business.listener

import be.inforius.takecareofyou.view.view_model.RecordViewModel

/**
 * Created by Cédric Denomerenge on 27-03-20
 */
interface OnScanCallbackListener {

    fun displayData(recordVM: RecordViewModel)
}