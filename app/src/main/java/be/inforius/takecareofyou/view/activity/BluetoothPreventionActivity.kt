package be.inforius.takecareofyou.view.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import be.inforius.takecareofyou.R
import kotlinx.android.synthetic.main.activity_bluetooth_prevention.*

class BluetoothPreventionActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bluetooth_prevention)

        bluetoothPrevention_validate_button.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
         when(v?.id){
             R.id.bluetoothPrevention_validate_button -> {
                 val intent = Intent(this, RegisterMobileActivity::class.java)
                 startActivity(intent)
             }
         }
    }
}
