package be.inforius.takecareofyou.view.util

import android.app.Activity
import android.view.inputmethod.InputMethodManager

class UiUtil {

    companion object{

        fun hideSoftKeyboard(view: Activity) {
            val inputMethodManager = view.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            view.currentFocus?.let { inputMethodManager.hideSoftInputFromWindow(it.windowToken, 0) }
        }
    }
}