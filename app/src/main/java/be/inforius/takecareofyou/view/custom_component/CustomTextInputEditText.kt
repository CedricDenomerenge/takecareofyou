package be.inforius.takecareofyou.view.custom_component

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import androidx.core.content.ContextCompat
import be.inforius.takecareofyou.R
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout

class CustomTextInputEditText : TextInputEditText, TextWatcher {

    constructor(context: Context): super(context)
    constructor(context: Context, attrs: AttributeSet): super(context,  attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int): super(context, attrs, defStyleAttr)

    private var typeFaceItalic = true
    private var isError = false

    init {
        this.addTextChangedListener(this)
    }

    fun setTextError(){

        isError = true
        if (text!!.isEmpty()) {
            setHintTextColor(ContextCompat.getColor(context, R.color.colorLightRed))
        }else {
            setTextColor(ContextCompat.getColor(context, R.color.colorRed))
        }
        (parent.parent as TextInputLayout).background = ContextCompat.getDrawable(context, R.drawable.custom_error_field_layout)
    }

    override fun afterTextChanged(s: Editable?) {

        if (isError) {
            setHintTextColor(ContextCompat.getColor(context, android.R.color.darker_gray))
            setTextColor(ContextCompat.getColor(context, R.color.colorTitle))
            (parent.parent as TextInputLayout).background = ContextCompat.getDrawable(context, R.drawable.custom_field_layout)
            isError = false
        }

        if (text!!.isEmpty() && !typeFaceItalic){
            //typeface = ResourcesCompat.getFont(context, R.font.source_sans_pro_italic)
            typeFaceItalic = true
        }else if (text!!.isNotEmpty() && typeFaceItalic) {
            //typeface = ResourcesCompat.getFont(context, R.font.source_sans_pro_regular)
            typeFaceItalic = false
        }
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

    }
}