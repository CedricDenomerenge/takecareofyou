package be.inforius.takecareofyou.view.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import be.inforius.takecareofyou.R

class ConsentRequirementActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_consent_requirement)
    }
}
