package be.inforius.takecareofyou.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import be.inforius.takecareofyou.R
import be.inforius.takecareofyou.model.entity.Record
import be.inforius.takecareofyou.view.view_model.RecordViewModel
import butterknife.BindView
import butterknife.ButterKnife

class RecordAdapter() : RecyclerView.Adapter<RecordAdapter.ViewHolder>() {

    private val MAX_COUNT = 4
    private val records = ArrayList<RecordViewModel>()

    fun addRecord(recordVM: RecordViewModel){
        if (records.count() >= MAX_COUNT){
            records.removeAt(0)
            notifyItemRemoved(0)
        }
        records.add(recordVM)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return records.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val inflater = LayoutInflater.from(parent.context)
        val v = inflater.inflate(R.layout.item_record, parent, false)

        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val data = records[position]

        holder.bind(data)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        @BindView(R.id.itemRecord_approachedModel)
        lateinit var approachedModel: TextView

        @BindView(R.id.itemRecord_macAddress)
        lateinit var macAddress: TextView

        @BindView(R.id.itemRecord_timestamp)
        lateinit var timestamp: TextView

        @BindView(R.id.itemRecord_rssi)
        lateinit var rssi: TextView

        @BindView(R.id.itemRecord_txPower)
        lateinit var txPower: TextView

        init {
            ButterKnife.bind(this, view)
        }

        fun bind(recordVM: RecordViewModel) {

            approachedModel.text = "Nom: ${recordVM.approachedModel}"
            macAddress.text = "MAC Adresse: ${recordVM.macAddress}"
            timestamp.text = "Timestamp: ${recordVM.timestamp}"
            rssi.text = "RSSI: ${recordVM.rssi}"
            txPower.text = "TXPower ${recordVM.txPower.toString()}"
        }
    }
}