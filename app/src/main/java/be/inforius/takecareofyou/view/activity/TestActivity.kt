package be.inforius.takecareofyou.view.activity

import android.Manifest
import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.content.*
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import be.inforius.takecareofyou.R
import be.inforius.takecareofyou.business.MyBluetoothGattServer
import be.inforius.takecareofyou.business.MyBluetoothHelper
import be.inforius.takecareofyou.business.MyBluetoothLeScanner
import be.inforius.takecareofyou.business.listener.OnScanCallbackListener
import be.inforius.takecareofyou.domain.constant.BluetoothConstants
import be.inforius.takecareofyou.domain.constant.RequestConstants
import be.inforius.takecareofyou.util.PermissionsUtil
import be.inforius.takecareofyou.view.adapter.RecordAdapter
import be.inforius.takecareofyou.view.view_model.RecordViewModel
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.LocationSettingsStatusCodes
import kotlinx.android.synthetic.main.activity_test.*

class TestActivity : AppCompatActivity(), View.OnClickListener, OnScanCallbackListener {

    private var isStartButton = true

    private lateinit var mRecordAdapter: RecordAdapter
    private var myBluetoothHelper: MyBluetoothHelper? = null
    private var myBluetoothGattServer: MyBluetoothGattServer? = null
    private var myBluetoothLeScanner: MyBluetoothLeScanner? = null
    private var isInit = false
    //private var mFirebaseService: FirebaseService? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)

        // Devices with a display should not go to sleep
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        test_startAndStop_button.setOnClickListener(this)

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            test_ble_status.text = "(BLE) Blutooth Lower Energy available"
            test_ble_status.setTextColor(getColor(R.color.colorGreen))
        }else{
            test_ble_status.text = "(BLE) Blutooth Lower Energy unavailable"
            test_ble_status.setTextColor(getColor(R.color.colorOrange))
        }

        initRecordRecyclerView()

        // Register for system Bluetooth events to start/stop services
        val filter = IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
        registerReceiver(mBluetoothReceiver, filter)

        // TODO Callback
        PermissionsUtil.verifyCoarseLocationPermission(this)
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(
                arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION), BluetoothConstants.PERMISSION_REQUEST_COARSE_LOCATION
            )
        }*/
    }

    private fun initBluetoothServices() {

        myBluetoothHelper = MyBluetoothHelper(this)

        // We can't continue without proper Bluetooth support
        if (!myBluetoothHelper!!.checkBluetoothSupport(myBluetoothHelper!!.bluetoothAdapter)) {
            Toast.makeText(this, "BLE is not available !", Toast.LENGTH_LONG).show()
            return
        }

        myBluetoothGattServer = MyBluetoothGattServer(this, myBluetoothHelper!!.bluetoothManager)
        myBluetoothLeScanner = MyBluetoothLeScanner(this, myBluetoothHelper!!.bluetoothAdapter, this)

        isInit = true

        startBluetoothServices()

    }

    private fun startBluetoothServices()  {

        println( "Bluetooth LE is enabled...starting services")

        Toast.makeText(this, "BLE is supported, start services...", Toast.LENGTH_SHORT).show()

        // Start advertising
        if ( ! myBluetoothGattServer!!.startAdvertising()){
            println("Failed to create advertiser")
            Toast.makeText(this, "Failed to create advertiser service !", Toast.LENGTH_LONG).show()
            changeButtonState(true)
            // TODO Finish.....
        }

        // Start scanner
        myBluetoothLeScanner!!.startsScan()
    }

    private fun stopBluetoothServices() {

        println( "Bluetooth LE stop services")

        Toast.makeText(this, "BLE stop services...", Toast.LENGTH_SHORT).show()

        if (myBluetoothHelper!!.bluetoothAdapter.isEnabled) {
            // Stop advertising
            myBluetoothGattServer!!.stopAdvertising()

            // Stop scanner
            myBluetoothLeScanner!!.stopScan()
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        stopBluetoothServices()

        unregisterReceiver(mBluetoothReceiver)
    }

    /**
     * Listens for Bluetooth adapter events to enable/disable
     * advertising and server functionality.
     */
    private val mBluetoothReceiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {

            val state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.STATE_OFF)

            when (state) {
                BluetoothAdapter.STATE_ON -> {
                    changeButtonState(false)
                    checkLocationActive()
                }
                BluetoothAdapter.STATE_OFF -> {
                    changeButtonState(true)
                    stopBluetoothServices()
                }
            }// Do nothing

        }
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.test_startAndStop_button -> {
                if (isStartButton) {
                    changeButtonState(false)
                    checkLocationActive()
                } else {
                    changeButtonState(true)
                    stopBluetoothServices()
                }
            }
        }
    }

    /**
     * Record Recycler instanciation with custom adapter
     */
    private fun initRecordRecyclerView(){

        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        test_recyclerView.layoutManager = layoutManager

        mRecordAdapter = RecordAdapter()
        test_recyclerView.adapter = mRecordAdapter

        val dividerItemDecoration = DividerItemDecoration(test_recyclerView.context, layoutManager.orientation)
        test_recyclerView.addItemDecoration(dividerItemDecoration)
    }

    private fun changeButtonState(isStart: Boolean){
        if(isStart){
            test_startAndStop_button.text = "Start"
            isStartButton = true
        }else{
            test_startAndStop_button.text = "Stop"
            isStartButton = false
        }
    }

    private fun checkBluetoothConnection() {

        if(MyBluetoothHelper(this).bluetoothAdapter.isEnabled){
            if (isInit) startBluetoothServices() else initBluetoothServices()
        }else{
            changeButtonState(true)
            Toast.makeText(this, "Il faut activer le bluetooth.", Toast.LENGTH_LONG).show()
        }
    }

    private fun checkLocationActive(){

        val mSettingsClient = LocationServices.getSettingsClient(this)
        val mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        val mLocationSettingsRequest = LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest).build()
        mSettingsClient.checkLocationSettings(mLocationSettingsRequest)
            .addOnSuccessListener(this) {
                checkBluetoothConnection()
            }
            .addOnFailureListener(this) { e ->
                val statusCode = (e as ApiException).statusCode
                when (statusCode) {
                    LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                        try {
                            val rae = e as ResolvableApiException
                            rae.startResolutionForResult(this@TestActivity, RequestConstants.REQUEST_CHECK_LOCATION_SETTINGS)
                        } catch (sie: IntentSender.SendIntentException) {
                            println("Error on location request: $sie")
                        }

                    }
                    LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                        val errorMessage = "Location settings are inadequate, and cannot be " + "fixed here. Fix in Settings."
                        Toast.makeText(this@TestActivity, errorMessage, Toast.LENGTH_LONG).show()
                    }
                }
            }
    }

    /**
     * Listener on MyBluetoothGattServer instance to listen on scanCallback with this activity
     */
    override fun displayData(recordVM: RecordViewModel) {

        mRecordAdapter.addRecord(recordVM)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if(requestCode == RequestConstants.PERMISSION_REQUEST_COARSE_LOCATION) {
            if (grantResults.isNotEmpty() && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                PermissionsUtil.verifyCoarseLocationPermission(this)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RequestConstants.REQUEST_CHECK_LOCATION_SETTINGS && resultCode == Activity.RESULT_OK) {
            checkBluetoothConnection()
        }
    }
}
