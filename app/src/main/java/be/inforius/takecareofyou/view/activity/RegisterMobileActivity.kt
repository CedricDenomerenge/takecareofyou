package be.inforius.takecareofyou.view.activity

import android.content.Intent
import android.os.Bundle
import android.os.SystemClock
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.content.ContextCompat
import be.inforius.takecareofyou.R
import be.inforius.takecareofyou.view.util.UiUtil
import kotlinx.android.synthetic.main.activity_register_mobile.*

class RegisterMobileActivity : AppCompatActivity(), TextWatcher, View.OnClickListener, TextView.OnEditorActionListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_mobile)

        registerMobile_number_textInputEditText.addTextChangedListener(this)

        registerMobile_getOtp_button.setOnClickListener(this)
        registerMobile_validateOtp_button.setOnClickListener(this)

        registerMobile_otpFirstNumber_textInputEditText.addTextChangedListener(
            CustomOTPTextWatcher(registerMobile_otpFirstNumber_textInputEditText))
        registerMobile_otpSecondNumber_textInputEditText.addTextChangedListener(
            CustomOTPTextWatcher(registerMobile_otpSecondNumber_textInputEditText))
        registerMobile_otpThirdNumber_textInputEditText.addTextChangedListener(
            CustomOTPTextWatcher(registerMobile_otpThirdNumber_textInputEditText))
        registerMobile_otpFourthNumber_textInputEditText.addTextChangedListener(
            CustomOTPTextWatcher(registerMobile_otpFourthNumber_textInputEditText))
    }

    private fun changeToOtpDisplay(){
        registerMobile_number_textInputEditText.isFocusable = false
        registerMobile_otp_layout.visibility = View.VISIBLE
        registerMobile_otpFirstNumber_textInputEditText.requestFocus()
        progress_loader_progressBar.progress = 50
        enableValidateButton(registerMobile_getOtp_button, false)
        registerMobile_getOtp_button.visibility = View.INVISIBLE
        registerMobile_validateOtp_button.visibility = View.VISIBLE
    }

    private fun enableValidateButton(button: AppCompatButton, enable: Boolean){
        if (enable){
            button.background = getDrawable(R.drawable.custom_button_layout)
            button.isEnabled = true
        }else{
            button.background = getDrawable(R.drawable.custom_disabled_button_layout)
            button.isEnabled = false
        }
    }

    private fun validatePhoneNumber(){
        //TODO Check si le numéro est valide
        if(registerMobile_number_textInputEditText.text.toString().length < 8){
            registerMobile_number_textInputLayout.error = getString(R.string.registerMobile_mobileNumberError_text)
        }else{
            changeToOtpDisplay()
        }
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.registerMobile_getOtp_button -> {
                validatePhoneNumber()
            }
            R.id.registerMobile_validateOtp_button -> {
                //val intent = Intent(this, ConsentRequirementActivity::class.java)
                val intent = Intent(this, TestActivity::class.java)
                startActivity(intent)
            }
        }
    }

    /**
     * En attente, voir si necessaire ( pas urgent )
     */
    override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
        if (event != null && (event.keyCode == KeyEvent.KEYCODE_ENTER)){
            if(actionId == EditorInfo. IME_ACTION_DONE) {
                UiUtil.hideSoftKeyboard(this)
                // Next activity "ConsentRequirementActivity"
            }else if(actionId == EditorInfo. IME_ACTION_SEND){
                validatePhoneNumber()
            }
            return true
        }
        return false
    }

    /**
     * Could not return back
     */
    override fun onBackPressed() {
        // Nothing to do here
    }

    override fun afterTextChanged(s: Editable?) {
        s?.let {
            registerMobile_number_textInputLayout.error = null
            if (registerMobile_number_textInputEditText.text.toString().length == 8){
                UiUtil.hideSoftKeyboard(this)
            }
        }
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

    inner class CustomOTPTextWatcher(private val view: View): TextWatcher {

        private val firstRef =  R.id.registerMobile_otpFirstNumber_textInputEditText
        private val secondRef =  R.id.registerMobile_otpSecondNumber_textInputEditText
        private val thirdRef =  R.id.registerMobile_otpThirdNumber_textInputEditText
        private val fourRef =  R.id.registerMobile_otpFourthNumber_textInputEditText

        override fun afterTextChanged(editable: Editable) { // TODO Auto-generated method stub
            val text = editable.toString()
            when (view.id) {
                firstRef -> if (text.isNotEmpty()) {
                    registerMobile_otpSecondNumber_textInputEditText.requestFocus()
                }
                secondRef -> if (text.isNotEmpty()) {
                    registerMobile_otpThirdNumber_textInputEditText.requestFocus()
                }else{
                    registerMobile_otpFirstNumber_textInputEditText.requestFocus()
                    registerMobile_otpFirstNumber_textInputEditText.setText("")
                }
                thirdRef -> {
                    if (text.isNotEmpty()) {
                        registerMobile_otpFourthNumber_textInputEditText.requestFocus()
                    }else{
                        registerMobile_otpSecondNumber_textInputEditText.requestFocus()
                        registerMobile_otpSecondNumber_textInputEditText.setText("")
                    }
                }
                fourRef -> if (text.isNotEmpty()){
                    // Validation OTP enable
                    //TODO Validation du matching de l'OTP Utilisateur et l'OTP calculer in App
                }else{
                    registerMobile_otpThirdNumber_textInputEditText.requestFocus()
                    registerMobile_otpThirdNumber_textInputEditText.setText("")
                }
            }
            isCompleteOTP()
        }

        private fun isCompleteOTP(){

            if(registerMobile_otpFirstNumber_textInputEditText.text.toString().length == 1
                && registerMobile_otpSecondNumber_textInputEditText.text.toString().length == 1
                && registerMobile_otpThirdNumber_textInputEditText.text.toString().length == 1
                && registerMobile_otpFourthNumber_textInputEditText.text.toString().length == 1){
                enableValidateButton(registerMobile_validateOtp_button, true)
                UiUtil.hideSoftKeyboard(this@RegisterMobileActivity)
            }else{
                enableValidateButton(registerMobile_validateOtp_button, false)
            }
        }

        override fun beforeTextChanged(arg0: CharSequence?, arg1: Int, arg2: Int, arg3: Int) {}

        override fun onTextChanged(arg0: CharSequence?, arg1: Int, arg2: Int, arg3: Int) {}
    }
}
