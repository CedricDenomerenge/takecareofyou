package be.inforius.takecareofyou.view.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import be.inforius.takecareofyou.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        main_validate_button.setOnClickListener(this)
    }

    override fun onClick(v: View?) {

        when(v?.id){
            R.id.main_validate_button -> {
                val intent = Intent(this, BluetoothPreventionActivity::class.java)
                startActivity(intent)

                // Lauch intent with ACTION_IGNORE_BATTERY_OPTIMIZATION_SETTINGS to ignore battery optimizer
            }
        }
    }
}
