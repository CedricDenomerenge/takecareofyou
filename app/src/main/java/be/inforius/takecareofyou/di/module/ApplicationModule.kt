package be.inforius.takecareofyou.di.module

import android.app.Application
import android.content.Context
import be.inforius.takecareofyou.di.scope.ApplicationContext
import dagger.Module
import dagger.Provides

@Module
class ApplicationModule(private val application: Application) {

    @Provides
    internal fun provideAppContext(): Application = this.application

    @Provides
    @ApplicationContext
    internal fun provideApplicationContext(): Context = application
}