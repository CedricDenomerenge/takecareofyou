package be.inforius.takecareofyou.di.module

import android.app.Application
import be.inforius.takecareofyou.model.helper.DBHelper
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ORMLiteModule constructor(private val application: Application) {

    @Provides
    @Singleton
    internal fun provideDBHelper(): DBHelper = DBHelper(application)
}