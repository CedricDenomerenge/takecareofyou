package be.inforius.takecareofyou.di.module

import android.app.Activity
import android.content.Context
import be.inforius.takecareofyou.di.scope.ActivityContext
import dagger.Module
import dagger.Provides

/**
 * Created by Cédric Denomerenge on 26-03-20
 */
@Module
class ActivityModule constructor(private val activity: Activity) {

    @Provides
    internal fun provideActivity(): Activity = activity

    @Provides
    @ActivityContext
    internal fun provideActivityContext(): Context = activity
}