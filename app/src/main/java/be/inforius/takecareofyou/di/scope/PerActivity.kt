package be.inforius.takecareofyou.di.scope

import javax.inject.Scope

/**
 * Created by Cédric Denomerenge on 26-03-20
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PerActivity