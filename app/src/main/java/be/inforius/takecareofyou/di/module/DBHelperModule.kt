package be.inforius.takecareofyou.di.module

import be.inforius.takecareofyou.model.helper.DBHelper
import be.inforius.takecareofyou.model.helper.RecordDBHelper
import be.inforius.takecareofyou.model.helper.StatusDBHelper
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DBHelperModule constructor(private val dbHelper: DBHelper) {

    @Provides
    @Singleton
    internal fun provideRecordDBHelper(): RecordDBHelper = RecordDBHelper(dbHelper)

    @Provides
    @Singleton
    internal fun provideStatusDBHelper(): StatusDBHelper = StatusDBHelper(dbHelper)

}