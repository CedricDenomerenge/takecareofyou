package be.inforius.takecareofyou.di.scope

import javax.inject.Qualifier

/**
 * Created by Cédric Denomerenge on 26-03-20
 */
@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class ActivityContext
