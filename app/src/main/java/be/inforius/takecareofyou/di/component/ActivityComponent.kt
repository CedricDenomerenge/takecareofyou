package be.inforius.takecareofyou.di.component

import android.content.Context
import be.inforius.takecareofyou.di.module.ActivityModule
import be.inforius.takecareofyou.di.scope.ActivityContext
import be.inforius.takecareofyou.di.scope.PerActivity
import be.inforius.takecareofyou.view.activity.TestActivity
import dagger.Component

@PerActivity
@Component(dependencies = [ApplicationComponent::class], modules = [ActivityModule::class])
interface ActivityComponent{

    @ActivityContext
    fun getContext(): Context

    //Future activities which have to inject service
    fun inject(testActivity: TestActivity)
}