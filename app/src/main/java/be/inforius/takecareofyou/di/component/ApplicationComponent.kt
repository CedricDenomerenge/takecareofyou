package be.inforius.takecareofyou.di.component

import android.app.Application
import android.content.Context
import be.inforius.takecareofyou.di.module.ApplicationModule
import be.inforius.takecareofyou.di.module.DBHelperModule
import be.inforius.takecareofyou.di.module.ORMLiteModule
import be.inforius.takecareofyou.di.scope.ApplicationContext
import be.inforius.takecareofyou.model.helper.DBHelper
import dagger.Component
import javax.inject.Singleton

/**
 * Created by Cédric Denomerenge on 26-03-20
 */
@Singleton
@Component(modules = [ApplicationModule::class, ORMLiteModule::class, DBHelperModule::class])
interface ApplicationComponent {

    @ApplicationContext
    fun getContext(): Context

    //fun sharedPreferencesHelper(): SharedPreferencesHelper

    fun dbHelper(): DBHelper

    //fun retrofitService(): RetrofitService

    fun inject(application: Application)
}