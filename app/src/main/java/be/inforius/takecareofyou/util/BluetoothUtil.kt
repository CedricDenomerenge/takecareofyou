package be.inforius.takecareofyou.util

class BluetoothUtil {

    companion object {

        /**
         * Convert RSSI to Meter distance
         */
        fun calculateAccuracy(txPower: Int, rssi: Int): Double {
            if (rssi == 0) {
                return -1.0 // if we cannot determine accuracy, return -1.
            }

            val ratio = rssi*1.0/txPower
            if (ratio < 1.0) {
                return Math.pow(ratio,10.0)
            }
            else {
                val accuracy = (0.89976)*Math.pow(ratio,7.7095) + 0.111
                return accuracy
            }
        }
    }
}