package be.inforius.takecareofyou.util

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import be.inforius.takecareofyou.domain.constant.RequestConstants

class PermissionsUtil {

    companion object {

        fun verifyCoarseLocationPermission(activity: Activity): Boolean {

            var granted = false

            val permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION)

            if (permission != PackageManager.PERMISSION_GRANTED) {
                // We don't have permission so prompt the user
                ActivityCompat.requestPermissions(
                    activity,
                    arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
                    RequestConstants.PERMISSION_REQUEST_COARSE_LOCATION)
            }else{
                granted = true
            }

            return granted
        }
    }
}