package be.inforius.takecareofyou.model.helper

import be.inforius.takecareofyou.model.entity.Record
import com.j256.ormlite.dao.Dao
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by Cédric Denomerenge on 09-01-20
 */
@Singleton
class RecordDBHelper @Inject constructor(dbHelper: DBHelper){

    private val mDao: Dao<Record, Int> = dbHelper.getDao(Record::class.java)

    fun getByid(id: Int): Record {
        return mDao.queryForId(id)
    }

    fun createOrUpdate(record: Record): Dao.CreateOrUpdateStatus? {
        return mDao.createOrUpdate(record)
    }
}