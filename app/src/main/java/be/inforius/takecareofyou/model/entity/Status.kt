package be.inforius.takecareofyou.model.entity

import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable

@DatabaseTable(tableName = "status_table")
class Status(

    @DatabaseField(generatedId = true)
    var id: Int,

    @DatabaseField(canBeNull = false)
    var timestamp: Long,

    @DatabaseField(canBeNull = false)
    var event: String){

    constructor() : this(0, 0,  "")
}