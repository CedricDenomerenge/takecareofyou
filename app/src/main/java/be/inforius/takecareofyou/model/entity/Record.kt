package be.inforius.takecareofyou.model.entity

import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable

@DatabaseTable(tableName = "record_table")
class Record (

    @DatabaseField(generatedId = true)
    var id: Int,

    @DatabaseField(canBeNull = false)
    var timestamp: Long,

    @DatabaseField(canBeNull = false)
    var peerId: String, // In base64

    @DatabaseField(canBeNull = false)
    var healthCenter: String,

    @DatabaseField(canBeNull = false)
    var approachedModel: String,

    @DatabaseField(canBeNull = false)
    var infectiousModel: String,

    @DatabaseField(canBeNull = false)
    var rssi: Int,

    @DatabaseField
    var txPower: Int?){

        constructor() : this(0, 0,  "", "", "",
        "",  0, null)
}