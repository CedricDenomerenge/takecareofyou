package be.inforius.takecareofyou.model.helper

import android.content.Context
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import be.inforius.takecareofyou.di.scope.ApplicationContext
import be.inforius.takecareofyou.domain.constant.DBConstants
import be.inforius.takecareofyou.model.entity.Record
import be.inforius.takecareofyou.model.entity.Status
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper
import com.j256.ormlite.dao.Dao
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.table.TableUtils
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DBHelper @Inject constructor(@ApplicationContext context: Context) :
    OrmLiteSqliteOpenHelper(context, DBConstants.DB_NAME, null, DBConstants.DB_VERSION) {

    init {
        //context.deleteDatabase(DBConstants.DB_NAME)
        writableDatabase
    }

    override fun onOpen(db: SQLiteDatabase?) {
        super.onOpen(db)
        setWriteAheadLoggingEnabled(true) // Multiple call simultaneously enabled
    }

    override fun onCreate(db: SQLiteDatabase, cs: ConnectionSource) {
        try {
            // Create Table with given table name with columnName
            TableUtils.createTable<Record>(cs, Record::class.java)
            TableUtils.createTable<Status>(cs, Status::class.java)

        } catch (e: SQLException) {
            throw RuntimeException(e)
        }
    }

    override fun onUpgrade(db: SQLiteDatabase, cs: ConnectionSource, oldVersion: Int, newVersion: Int) {

        // Nothing to do here
    }
}