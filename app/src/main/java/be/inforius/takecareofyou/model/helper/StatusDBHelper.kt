package be.inforius.takecareofyou.model.helper

import be.inforius.takecareofyou.model.entity.Status
import com.j256.ormlite.dao.Dao
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by Cédric Denomerenge on 09-01-20
 */
@Singleton
class StatusDBHelper @Inject constructor(dbHelper: DBHelper){

    private val mDao: Dao<Status, Int> = dbHelper.getDao(Status::class.java)

    fun getByid(id: Int): Status {
        return mDao.queryForId(id)
    }

    fun createOrUpdate(status: Status): Dao.CreateOrUpdateStatus? {
        return mDao.createOrUpdate(status)
    }
}