package be.inforius.takecareofyou.model.shared_preferences

import android.content.Context
import android.content.SharedPreferences
import be.inforius.takecareofyou.BaseApplication
import be.inforius.takecareofyou.domain.constant.SPConstants

/**
 * Created by Cédric Denomerenge on 24-04-19
 */

class SharedPreferencesHelper {

    private var mPref: SharedPreferences =
        BaseApplication.context.getSharedPreferences(SPConstants.SP_FILE_NAME, Context.MODE_PRIVATE)

    companion object {

        private var mInstance: SharedPreferencesHelper? = null

        @Synchronized
        fun getInstance(): SharedPreferencesHelper =
            mInstance ?: synchronized(this){
                mInstance ?: SharedPreferencesHelper()
            }
    }

    fun isMobileRegistered(): Boolean{
        return mPref.getBoolean(SPConstants.IS_MOBILE_REGISTERED, false)
    }
}