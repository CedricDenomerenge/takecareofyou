package be.inforius.takecareofyou.domain.constant

import java.util.*

/**
 * Created by Cédric Denomerenge on 16-05-19
 */
class DBConstants {

    companion object {

        const val DB_NAME = "takecareofyou_database.db"
        const val DB_VERSION = 1
    }
}