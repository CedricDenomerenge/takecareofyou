package be.inforius.takecareofyou.domain.constant

/**
 * Created by Cédric Denomerenge on 19-04-19
 */
class RequestConstants {

    companion object {

        const val REQUEST_CHECK_LOCATION_SETTINGS = 11
        const val PERMISSION_REQUEST_COARSE_LOCATION = 101

    }
}