package be.inforius.takecareofyou.domain.constant

class BluetoothConstants {

    companion object{
        const val CUSTOM_SERVICE_UUID = "00008888-0000-1000-8000-00805f9b34fb"
        const val PERMISSION_REQUEST_COARSE_LOCATION = 456
    }
}