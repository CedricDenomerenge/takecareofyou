package be.inforius.takecareofyou.domain.constant

class SPConstants {

    companion object{

        const val SP_FILE_NAME = "takecareofyou_preferences"
        const val IS_MOBILE_REGISTERED = "is_mobile_registered"
    }
}